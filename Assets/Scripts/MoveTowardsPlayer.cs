﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowardsPlayer : MonoBehaviour {
    private Transform player;
    [Header("Enemy Properties")]
    public float speed = 2.0f;
    public int collisionDamage = 2;
    // Use this for initialization
    void Start()
    {
        // Has to be this way rather than a
        // serialised field as this is will
        // be a prefab ( when we make waves
        // of enemy ships .
        player = GameObject.Find("PlayerShip").transform;
    }

    // Update is called once per frame
    void Update () {
        // Check the player attribute was set in Start ().
        if (player == null)
        {
            Debug.Log(" Cannot find player ship , please " +
            " check its called \" PlayerShip \" ");
        }
        else // This else is repeated from the code above
        {
            // Get the distance the players position to ours .
            Vector3 vectorToPlayer = player.position -
            transform.position;
            // We want the direction only , not the magnitude
            vectorToPlayer.Normalize();
            // distance to move , scaled based on time since last update .
            float dist = speed * Time.deltaTime;
            // Move the enemy
            Vector3 movement = (vectorToPlayer * dist);
            transform.position = transform.position + movement;
        }
        }
    }
