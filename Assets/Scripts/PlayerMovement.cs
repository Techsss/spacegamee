﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PlayerMovement : MonoBehaviour{
    // Movement modifier applied to directional movement
    public float playerSpeed = 4.0f;
    // Speed decay over time ( how fast we slow down )
    public float speedDecay = 0.9f;
    // Current player speed
    private float currentSpeed;
    // Use this for initialization
    void Start(){
    }
    // Update is called once per frame
    void Update(){
        updateRotation();
        updatePosition();
    }
    void updateRotation(){
        // Work out where the mouse is relative to the player .
        // current mouse coordinates in screen space
        Vector3 worldPos = Input.mousePosition;
        // convert screen coords to world space coords .
        worldPos = Camera.main.ScreenToWorldPoint(worldPos);
        // Calculate the difference (a vector )
        // deltaVec = ourPos - mousePos
        float dx = this.transform.position.x - worldPos.x;
        float dy = this.transform.position.y - worldPos.y;
        // Calculate the angle between the two - using trig
        // this gives us radians
        float angle = Mathf.Atan2(dy, dx);
        // convert to degrees
        angle = angle * Mathf.Rad2Deg;
        // The ships rotation is stored as a quaternion ,
        // we need to make a new one its a rotation around the
        //z axis , we create this from a vector representing
        // the Euler angles ( rotAroundX , rotAroundY , rotAroundZ )
        //+ 90? - our sprite is not straight on.
        Quaternion rot = Quaternion.Euler(new Vector3(0, 0, angle + 90));
        // Set this rotation on the ship
        this.transform.rotation = rot;
    }
    void updatePosition(){
        // Movement to apply for this frame
        Vector3 movement = new Vector3();
        // Check for input , use the generalised input system .
        movement.x += Input.GetAxis("Horizontal");
        movement.y += Input.GetAxis("Vertical");
        /* Note : if we pressed multiple buttons at the same time ,
        * make sure we ’re only moving the same lenght
        */
        movement.Normalize();
        // Before updating the player , make sure
        // there is something to add .
        if (movement.magnitude > 0)
        {
            // Lenght of the vector is greater than 0 , we
            // have a movement to add .
            currentSpeed = playerSpeed;
        }
        else
        {
            // Slow down over time
            currentSpeed *= speedDecay;
        }
        // Scale accoring to speed and deltaTime
        //( time since last update ).
        movement = movement * (Time.deltaTime * currentSpeed);
        // Translate ( move ) this object (in world coordinates ).
        this.transform.Translate(movement, Space.World);
    }
}
