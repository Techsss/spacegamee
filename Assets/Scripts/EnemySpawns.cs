﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawns : MonoBehaviour
{
    public int spawnTime = 10;
    public GameObject Enemy;
    public Transform player;
    public void Start()
    {
        StartCoroutine(WaitThenSpawn());
    }
    IEnumerator WaitThenSpawn()
    {
        yield return new WaitForSeconds(spawnTime);
        float spawnPointX = UnityEngine.Random.Range(-10, 10);
        float spawnPointY = UnityEngine.Random.Range(-10, 10);
        Vector2 spawnPosition = new Vector2(spawnPointX + this.transform.position.x, spawnPointY + this.transform.position.y);

        Instantiate(Enemy, spawnPosition, player.rotation);
        StartCoroutine(WaitThenSpawn());
    }
}
