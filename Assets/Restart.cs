﻿
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    public void RestartGame()
    {
        ScoreScript.scoreValue = 0;
        SceneManager.LoadScene("SampleScene");
    }
}
